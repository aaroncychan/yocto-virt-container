# yocto-virt-container

An Open Source Project based on Yocto Project Containers using CROPS, LAVA, and Docker Containers
Model of integrated solutions into Jenkins, Buildbot, Linaro (LAVA) as Consolidated Container Infrastructure, CCI

# Jenkins - Docker Build Successful a Tag created as crops/<distro><version>-buildid-<build_number>

$ docker images
REPOSITORY                     TAG                 IMAGE ID            CREATED             SIZE
crops/ubuntu16.04-buildid-67   latest              b7ce3682536e        29 seconds ago      1.42GB
crops/ubuntu16.04-buildid-66   latest              629b3b59b045        5 hours ago         1.42GB
crops/ubuntu16.04-buildid-65   latest              ae3a24b2d849        22 hours ago        1.42GB
crops/ubuntu16.04-buildid-64   latest              bc164a28e5f5        23 hours ago        1.42GB

# Docker Run Images

This wrapper runs the docker image on the background by indicating tag 

$ docker-run-images crops/ubuntu16.04-buildid-67

1. Enter your new password on remote access for VNCserver
2. PIP install buildbot-worker
3. Instantiate a CROPS container

# Docker Clear Containers

This wrapper script cleans up the docker containers with STATUS expression has "Exited ()... minutes ago"

$ docker-clean-containers
894084144fc1f5ed088da095151e1b00bf940f8cdf59e0bcc347b9f75ec3cc2f
f5fbd300fca42fb8cf2fa06706b5b06d82b9d10022b9cec2768b5ee660670fee
cd6c42f539b41196485fb7d0a18bf5f130832056ad745fb15b9f46ad9dcbec04
26ae82f2d6b62cd6f91b5d8cec2605251a73de708eb9023640a9e5f908f4230a
bc3ae6a9bd9b367596221be3e1ef4737a6b01891fa5e2b9b1d2428571c6808a0
468556b2b28ecc29a60af1c5d0f5baca0586b97a5e5f87850cfdcd2a580400b8
1d26cc1c294203f847adc117f7a3746c70503a0c72ff9be9646507149bca3d2b
150e7af38c29fe70503e8c31b30afbec0205a889056731e3c756a9e0630a3224
f1e109fa69ea11681cfdfac3fd7dc0084f1e034067a12bbe32bba9acc1769ad4
All Docker Containers Cleaned-Up Successfully